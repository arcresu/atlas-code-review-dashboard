ATLAS code review dashboard
===========================

This tool was used to assist developers during code review shifts.
It uses the GitLab API to detect common problems that code reviewers
needed to address to help them spend their effort most effectively.

The problems dashboard highlighted merge requests that were in an
unclear state in the review process or hadn't seen recent activity:

![Problems dashboard](screenshot1.png)

The queue dashboard showed the number of open merge requests in each
phase of the review process. This helped to assess when the backlog
was growing too big and more shifts were needed:

![Queue dashboard](screenshot2.png)
