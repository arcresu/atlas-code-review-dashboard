#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Author: Carl Suster <carl.suster@cern.ch>
# Works with Python 2.7 or 3.
# Depends: python-gitlab
#
# Checks MRs in atlas/athena for a number of issues.
# Issues are presented as an actionable list for merge review shifters.

from __future__ import print_function, with_statement

import sys, os.path
import datetime
import argparse
import logging

import gitlab

from gitlab_api_helpers import AtlasMergeRequest, ATLAS_BOT


class Problem:
    tag = None

    def __init__(self):
        self.source = None

    def __str__(self): return '{}'.format(self.name)

    @property
    def name(self):
        return self.__class__.tag

    @property
    def description(self):
        desc = self.__doc__ or '{}({})'.format(self.__class__.__name__, self.name)
        return desc.strip()

    def managed(self):
        ''' If true, the problem has been acted on. '''
        return False

    def as_html(self):
        return ''

class BigQueue(Problem):
    """There are more than 100 MRs in flight (open but excluding WIP).
    This means we're accumulating a backlog and will be slower to notice forgotten MRs.
    Consider checking the full list of open MRs for any non-WIP ones that might be in the wrong queue, overlooked by the CI/bot or otherwise stuck.
    If MRs have been open for a long time without activity it might be appropriate to ask if they can be closed or marked as WIP."""
    tag = 'big-queue'
    threshold = 100

class BigL1Queue(Problem):
    """There are more than 50 MRs in the L1 queue.
    The L1 queue is starting to become a bottleneck in the pipeline.
    After urgent MRs, consider prioritising sweeps and small MRs that change config files or fix compiler warnings to clear things up."""
    tag = 'big-l1-queue'
    threshold = 50

class ForkProblem(Problem):
    template = None

    def __init__(self, fork, mrs=[]):
        Problem.__init__(self)
        self.fork = fork
        self.mrs = mrs
        self.source = ('fork', fork)

    def __str__(self):
        fork_name = self.fork.path_with_namespace if self.fork else '?'
        return '{}[{},{}]'.format(self.name, fork_name, ','.join(map(str, self.mrs)))

    def __add__(self, other):
        return self.__class__(self.fork, self.mrs + other.mrs)

    def as_html(self):
        return fork_as_html(self.fork, self.mrs)

class BotLevel(ForkProblem):
    """The atlas bot has been added to the fork but does not have 'Developer' permissions.
    Remind the developer to <a href=https://atlassoftwaredocs.web.cern.ch/gittutorial/gitlab-fork/#add-your-friendly-build-bot>add the atlas bot</a> as a Developer to their fork."""
    tag = 'atlasbot-level'
    correct_level = 30 # Developer

class BotMissing(ForkProblem):
    """The atlas bot is not in the fork's members.
    Remind the developer to <a href=https://atlassoftwaredocs.web.cern.ch/gittutorial/gitlab-fork/#add-your-friendly-build-bot>add the atlas bot</a> as a Developer to their fork."""
    tag = 'atlasbot-missing'

class ForkUnreachable(ForkProblem):
    """The fork is not reachable.
    This can mean that the fork is set to Private (instead of Internal) or has been deleted, in which case this should be fixed by the submitter, or there could be infrastructure issues in which case this should go away by itself."""
    tag = 'unreachable'

class MRProblem(Problem):
    types = []

    def __init__(self, mr):
        Problem.__init__(self)
        self.mr = mr
        self.source = ('mr', mr)

    def __str__(self):
        return '{}[{}]'.format(self.name, self.mr)

    def as_html(self):
        return mr_as_html(self.mr, self.managed())

    @staticmethod
    def applies_to(mr, cgl=None):
        raise NotImplemented

    @staticmethod
    def enable(tag):
        def decorator(defcls):
            MRProblem.types.append(defcls)
            defcls.tag = tag
            return defcls
        return decorator

@MRProblem.enable('self-target')
class SelfTarget(MRProblem):
    """The MR redundantly targets its own target branch for sweeping.
    This will produce a sweep failure when the sweep script attempts to sweep the MR into the branch it was already merged into, so the label should be removed now."""

    @staticmethod
    def applies_to(mr, cgl=None):
        return mr.target_branch in mr.other_targets

# @MRProblem.enable('failed-sweep') FIXME
class FailedSweep(MRProblem):
    """The MR was not successfully swept.
    The submitted should follow up and make sure that after all sweeps are done the label is changed to reflect that."""

    @staticmethod
    def applies_to(mr, cgl=None):
        return mr.sweep_problem

@MRProblem.enable('labelled-wip')
class LabelledWIP(MRProblem):
    """The MR is WIP but has review labels.
    WIP means that an MR is not ready for review, so you can remove any review labels."""

    @staticmethod
    def applies_to(mr, cgl=None):
        return (mr.wip and mr.review_labels and not mr.approved)

@MRProblem.enable('only-expert')
class OnlyExpert(MRProblem):
    """ The MR is assigned to experts and might get lost.
    Check that there is a specific expert we're waiting for - if nobody has clear responsibility this MR will stagnate.
    If there is someone specific mentioned in the MR discussion, they might need a ping."""

    @staticmethod
    def applies_to(mr, cgl=None):
        return (mr.expert and mr.review_pending and len(mr.review_labels) == 1)

@MRProblem.enable('cannot-merge')
class CannotBeMerged(MRProblem):
    """The MR cannot be merged according to GitLab.
    This can mean that there are merge conflicts: consider pointing to <a href="https://atlassoftwaredocs.web.cern.ch/gittutorial/resolve-conflict/">the guide</a> to resolve.
    It could also be that the source branch is missing in which case the submitter should restore it or the MR should be closed.
    Resolution: WIP or labelled as requiring user action."""

    @staticmethod
    def applies_to(mr, cgl=None):
        return mr.cannot_merge

    def managed(self):
        return self.mr.waiting or self.mr.wip

@MRProblem.enable('invisible')
class Invisible(MRProblem):
    """The MR has not been commented on by the bot after 2 hours.
    This could mean that there's a CI backlog and everything is fine, or it could mean that something about the MR is stopping the bot from reacting.
    You should add the appropriate review label so that the MR doesn't get lost.
    Resolution: has a review label."""

    threshold = datetime.timedelta(hours=2)

    @staticmethod
    def applies_to(mr, cgl):
        return (mr.created > Invisible.threshold
                and not mr.wip
                and not mr.check_bot_has_commented(cgl))

    def managed(self):
        return len(self.mr.review_labels) > 0

@MRProblem.enable('l1-and-waiting')
class L1AndWaiting(MRProblem):
    """The MR is redundantly labelled for L1 review and user action.
    Either the L1 label or the user label should be removed."""

    @staticmethod
    def applies_to(mr, cgl=None):
        return (mr.l1 and mr.waiting and not mr.wip)

@MRProblem.enable('maybe-approved')
class MaybeApproved(MRProblem):
    """The MR is approved but also has other review labels.
    Either the approval or other labels should probably be removed."""

    @staticmethod
    def applies_to(mr, cgl=None):
        if mr.approved:
            if mr.cannot_merge and mr.waiting and len(mr.review_labels) == 2:
                return False
            return len(mr.review_labels) > 1

@MRProblem.enable('stale-wip')
class StaleWIP(MRProblem):
    """The MR is WIP and has not been updated in 3 weeks.
    Consider pinging the submitter for an update or checking whether the MR can be closed."""

    threshold = datetime.timedelta(weeks=3)

    @staticmethod
    def applies_to(mr, cgl=None):
        return (mr.wip and mr.updated > StaleWIP.threshold)

@MRProblem.enable('stale-waiting')
class StaleWaiting(MRProblem):
    """The MR is waiting for the submitter and has not been updated in 3 weeks.
    Consider pinging them for an update or checking whether the MR can be closed."""

    threshold = datetime.timedelta(weeks=3)

    @staticmethod
    def applies_to(mr, cgl=None):
        return (mr.waiting and mr.updated > StaleWaiting.threshold)

@MRProblem.enable('stale-urgent')
class StaleUrgent(MRProblem):
    """The MR is urgent and has not been updated in 1 day.
    Prioritise this MR or remove the urgent label if it's no longer relevant."""

    threshold = datetime.timedelta(days=1)

    @staticmethod
    def applies_to(mr, cgl=None):
        return (mr.urgent and mr.updated > StaleUrgent.threshold)

@MRProblem.enable('stale-approved')
class StaleApproved(MRProblem):
    """The MR is approved and has not been updated in 3 weeks.
    The release coordinator for the branch can be deduced by looking at MRs recently merged into the target branch.
    They might need a ping unless there is a reason explained in the MR discussion for why the MR is postponed."""

    threshold = datetime.timedelta(weeks=3)

    @staticmethod
    def applies_to(mr, cgl=None):
        return (mr.approved
                and mr.updated > StaleApproved.threshold
                and not mr.wip)

@MRProblem.enable('unlabelled')
class Unlabelled(MRProblem):
    """The MR has no review labels and is not WIP.
    Somebody might have accidentally removed the review labels.
    You should add the appropriate review labels so that the MR doesn't get lost."""

    @staticmethod
    def applies_to(mr, cgl):
        return (not (mr.review_labels or mr.wip)
                and mr.check_bot_has_commented(cgl))

@MRProblem.enable('unreviewed')
class Unreviewed(MRProblem):
    """The MR has been waiting over 1 day for review.
    Ideally this shouldn't happen while there are MR shifters."""

    threshold = datetime.timedelta(days=1)

    @staticmethod
    def applies_to(mr, cgl=None):
        return (mr.review_pending and mr.updated > Unreviewed.threshold)

def get_problems(site, token, project_name):
    ''' Checks all open MRs for certain problems. '''
    cgl = gitlab.Gitlab(site, token, api_version = 4)

    cgl.auth()
    logging.info("Successfully authenticated as {}".format(cgl.user.username))

    project = cgl.projects.get(project_name, lazy=True)
    logging.debug("retrieved GitLab project handle")

    problems = []

    n_mr = 0
    n_mr_in_flight = 0
    n_l1 = 0
    logging.info('Fetching and checking open merge requests')
    for mr in project.mergerequests.list(state = 'opened', as_list = False):
        mr = AtlasMergeRequest(mr)
        n_mr += 1
        if not mr.wip: n_mr_in_flight += 1
        if mr.l1: n_l1 += 1
        fork = mr.get_fork(cgl)

        if not fork:
            problems.append(ForkUnreachable(fork, [mr]))
            continue
        for dev in fork.members.list(as_list = False):
            if dev.username == ATLAS_BOT:
                if dev.access_level != BotLevel.correct_level:
                    problems.append(BotLevel(fork, [mr]))
                break
        else:
            problems.append(BotMissing(fork, [mr]))

        for problem_cls in MRProblem.types:
            if problem_cls.applies_to(mr, cgl):
                problems.append(problem_cls(mr))

    if n_mr_in_flight > BigQueue.threshold:
        problems.append(BigQueue())
    if n_l1 > BigL1Queue.threshold:
        problems.append(BigL1Queue())

    for problem in problems:
        logging.debug('{!s}'.format(problem))
    logging.info('Found {} problems'.format(len(problems)))
    return problems

def merge_problems(problems):
    merged = []
    by_fork = {}

    for problem in problems:
        if problem.source and problem.source[0] == 'fork':
            pn_fn = (problem.name, problem.source[1])
            if pn_fn in by_fork:
                by_fork[pn_fn] += problem
            else:
                by_fork[pn_fn] = problem
        else:
            merged.append(problem)

    return merged + by_fork.values()

HTML = """
<!-- Generated by lint_merge_requests.py -->
<!doctype html><html>
<head>
  <title>MR problems : atlas/athena</title>
  <meta charset=UTF-8 />
  <meta name=viewport content="width=device-width, initial-scale=1" />
  <style type=text/css>
    body { margin: 0; font-family: palatino serif; }
    .problem-name { font-family: mono; font-weight: bold; }
    .problem-name::before { content: "["; }
    .problem-name::after { content: "]"; }
    .tooltip { position: relative; display: inline-block; }
    .tooltip + .tooltip-text {
        display: none; background: rgba(0,0,0,0.8); color: white;
        padding: 10px; border-radius: 5px; position: absolute; z-index: 1;
        max-width: 400px; margin-left: 1ex; font-weight: normal; }
    .tooltip + .tooltip-text a { color: white; }
    .tooltip:hover + .tooltip-text { display: block;; }
    header { padding: 5vh 2vw; background: #ddd; }
    header h1 { margin-top: 0; }
    .problems {
        display: flex; flex-flow: row wrap; justify-content: space-between;
        margin: 5vh 2vw; }
    .problems.by-source { justify-content: space-around; }
    .problem-group {
        clear: both; margin: 1em 2vw; border-bottom: solid 1px #ddd; }
    .by-source .problem-group { flex: 1 350px; }
    .by-type .problem-group { flex: 0.5 750px; }
    .group-heading  {
        float: right; clear: both; display: inline-block; margin-left: 1em;
        margin-top: 0; font-size: 100%; }
    .group-heading + .problem-doc { margin-bottom: 0.5em; margin-top: 0; }
    .problem-list {
        overflow: auto; padding-left: 0; clear: left; margin-top: 0; }
    .problem-instance {
        float: left; display: inline-block; margin: 0.5ex; padding: 0.5mm 0; }
    .source {
        padding: 1.5mm; padding-top: 0.5mm; border-radius: 5mm; color: black;
        font-weight: normal; font-family: mono; }
    .source.type-mr {
        background-color: cyan; box-shadow: inset 0 -2px 0 1px rgb(0,60,140);
        border-top: solid 1px rgba(0,60,140,0.2); }
    .source.type-fork {
        background-color: magenta; box-shadow: inset 0 -2px 0 1px rgb(140,0,60);
        border-top: solid 1px rgba(140,0,60,0.2); display: block; color: white; }
    .fork-mrs {
        list-style: none; padding: 0; display: flex;
        justify-content: space-evenly; }
    .fork-mr .source.type-mr {
        border-radius: 0; border: none; box-shadow: none; padding: 0;
        background: none; font-size: 75%; }
    .fork-mr .tooltip:hover + .tooltip-text { display: none; }
    .source.problem-managed { opacity: 0.2; }
    .source.problem-managed::after { content: " ✔"; }
    .source:hover { filter: invert(100%); }
    a.source, .source a { text-decoration: none; color: inherit; }
  </style>
<body>
  <header>
    <h1>Merge request problems : atlas/athena</h1>
"""

def mr_as_html(mr, managed=False):
    classes = ['type-mr', 'source', 'tooltip']
    if managed:
        classes.append('problem-managed')
    return '<a class="{css_class}" href="{mr_url}">!{mr_id}</a><span class=tooltip-text>{mr_title}</span>'.format(
            mr_url = mr.url,
            mr_id = mr.number,
            mr_title = mr.title,
            css_class = ' '.join(classes))

def fork_as_html(fork, mrs=[]):
    if fork:
        fork_html = '<span class="type-fork source"><a class=fork-name href="{fork_url}">{fork_name}</a>'.format(
                fork_name = fork.path_with_namespace,
                fork_url = fork.web_url)
    else:
        fork_html = '<span class="type-fork source"><span class=fork-name>???</span>'
    if mrs:
        fork_html += '<ul class=fork-mrs>'
        for mr in mrs:
            fork_html += '<li class=fork-mr>' + mr_as_html(mr)
        fork_html += '</ul>'
    return fork_html + '</span>'

def generate_by_source_html(problems, results):
    ''' Create an HTML status page grouped by problem source. '''

    data = {}
    for problem in problems:
        if problem.source not in data:
            data[problem.source] = []
        data[problem.source].append(problem)

    problem_file = os.path.join(results, 'mr-problems-bs.html')
    logging.info("Writing summary to: {}".format(problem_file))
    with open(problem_file, 'w') as pf:

        # List all outstanding problems:
        pf.write(HTML)
        pf.write('<p>Last updated: {:%Y-%m-%d <b>%H:%M</b>} (CERN time)</p>\n'.format(datetime.datetime.now()))
        pf.write('<p>This is a list of automatically detected problems with merge requests against <a href=https://gitlab.cern.ch/atlas/athena/merge_requests>atlas/athena</a>.</p>\n')
        pf.write('<p>See <a href=mr.html>MR queue occupancy</a> for plots of the number of open MRs in various states.</p>\n')
        pf.write('<p>See also <a href=mr-problems-bp.html>MR problems by problem</a>.</p>\n')
        pf.write('</header>\n')
        pf.write('<section class="problems by-source">\n')
        if problems:
            for source, instances in sorted(data.items(),
                    key=lambda (s,i): s and str(s[1])):
                src = instances[0].source
                pf.write('<article class=problem-group>\n')
                pf.write('<h1 class=group-heading>')
                if src and src[0] == 'mr':
                    pf.write(mr_as_html(src[1]))
                elif src and src[0] == 'fork':
                    pf.write(fork_as_html(src[1]))
                else:
                    pf.write('&nbsp;')
                pf.write('</h1><ul class=problem-list>\n')
                for problem in instances:
                    pf.write('<li class=problem-instance><span class="tooltip problem-name">{}</span><span class=tooltip-text>{}</span></li>\n'.format(
                        problem.name, problem.description))
                pf.write('</ul>\n')
                pf.write('</article>\n')
        else:
            pf.write('<p>No problems outstanding for now.</p>')
        pf.write('</section>\n')

def generate_by_problem_html(problems, results):
    ''' Create an HTML status page grouped by problem type. '''

    data = {}
    for problem in problems:
        if problem.name not in data:
            data[problem.name] = []
        data[problem.name].append(problem)

    problem_file = os.path.join(results, 'mr-problems-bp.html')
    logging.info("Writing summary to: {}".format(problem_file))
    with open(problem_file, 'w') as pf:

        # List all outstanding problems:
        pf.write(HTML)
        pf.write('<p>Last updated: {:%Y-%m-%d <b>%H:%M</b>} (CERN time)</p>\n'.format(datetime.datetime.now()))
        pf.write('<p>This is a list of automatically detected problems with merge requests against <a href=https://gitlab.cern.ch/atlas/athena/merge_requests>atlas/athena</a>.</p>\n')
        pf.write('<p>See <a href=mr.html>MR queue occupancy</a> for plots of the number of open MRs in various states.</p>\n')
        pf.write('<p>See also <a href=mr-problems-bs.html>MR problems by source</a>.</p>\n')
        pf.write('</header>\n')
        pf.write('<section class="problems by-type">\n')
        if problems:
            for problem, instances in sorted(data.items()):
                pf.write('<article class=problem-group>\n')
                pf.write('<h1 class="problem-name group-heading">{}</h1>\n'.format(problem))
                pf.write('<p class=problem-doc>{}</p>\n'.format(instances[0].description))
                if any(filter(lambda x: x.source is not None, instances)):
                    pf.write('<ul class=problem-list>\n')
                    for instance in instances:
                        html = instance.as_html() or str(instance)
                        pf.write('<li class=problem-instance>' + html + '</li>\n')
                pf.write('</ul>\n</article>\n')
        else:
            pf.write('<p class=problem-none>No problems outstanding for now.</p>')
        pf.write('</section>\n')


def main():
    parser = argparse.ArgumentParser(description="GitLab queue monitor",formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p','--project-name',dest='project_name', default='atlas/athena', help="GitLab project with namespace (e.g. user/my-project)")
    parser.add_argument('-t','--token',required=True,help="private GitLab user token")
    parser.add_argument('-u','--url',default='https://gitlab.cern.ch',help="URL of GitLab instance")
    parser.add_argument('--results',default='./mr-lint-results',help="Directory to store HTML")
    parser.add_argument('-v','--verbose',default='INFO',choices=['DEBUG','INFO','WARNING','ERROR','CRITICAL'],help="verbosity level")

    # get command line arguments
    args = parser.parse_args()

    # configure log output
    logging.basicConfig(format='%(asctime)s %(levelname)-10s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.getLevelName(args.verbose))

    logging.debug("parsed arguments:\n" + repr(args))
    logging.info("Using project {} from {}".format(args.project_name, args.url))

    problems = get_problems(args.url, args.token, args.project_name)
    problems = merge_problems(problems)

    generate_by_source_html(problems, args.results)
    generate_by_problem_html(problems, args.results)

if __name__ == '__main__':
    sys.exit(main())
