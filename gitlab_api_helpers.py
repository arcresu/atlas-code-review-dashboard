# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

import logging
import time, datetime
import re

from gitlab import GitlabGetError

# special usernames
ATLAS_BOT = 'atlasbot'


def gitlab_time_ago(timestamp):
    ''' Converts GitLab timestamp into seconds before now, assuming local time. '''

    dt, zoneh, zonem = re.search(r'^(.+?)(?:[+-](\d\d):(\d\d))?$', timestamp).groups()
    updated = time.strptime(dt, '%Y-%m-%dT%H:%M:%S.%f')
    zone = datetime.timedelta(hours=int(zoneh), minutes=int(zonem))
    return datetime.datetime.now() - datetime.datetime.fromtimestamp(time.mktime(updated)) - zone


# Definitions of labels with special meanings for the ATLAS review workflow:
LABEL_L1 = 'review-pending-level-1'
LABEL_L2 = 'review-pending-level-2'
LABEL_EXPERT = 'review-pending-expert'
LABEL_WAITING = 'review-user-action-required'
LABEL_APPROVED = 'review-approved'
LABEL_URGENT = 'urgent'

LABELS_REVIEW = r'^review-(.+)$'
LABELS_TARGETING = r'^alsoTargeting:(.+)$'
LABELS_SWEPT_FROM = r'^sweep:from (.+)$'

class AtlasMergeRequest:
    ''' Wrapper around GitLab MR object to provide ATLAS-specific convenience. '''

    def __init__(self, gitlab_mr):
        self.update(gitlab_mr)

    def __str__(self):
        return '!' + str(self.number)

    def update(self, gitlab_mr):
        ''' Updates properties to reflect the provided GitLab MR object. '''

        self._raw = gitlab_mr

        self.title = self._raw.title.encode('utf-8')
        self.labels = self._raw.labels
        self.wip = self._raw.work_in_progress
        self.target_branch = self._raw.target_branch
        self.updated = gitlab_time_ago(self._raw.updated_at)
        self.created = gitlab_time_ago(self._raw.created_at)
        self.username = self._raw.author['username']
        self.number = self._raw.iid
        self.url = self._raw.web_url
        self.open = self._raw.state in ['opened', 'reopened']
        self.cannot_merge = self._raw.merge_status == 'cannot_be_merged'

        self.review_labels = []
        self.other_targets = []
        self.other_targets_labels = []
        self.swept_from = None
        self.swept_from_label = None
        self.other_labels = []
        for label in self.labels:
            match_r = re.search(LABELS_REVIEW, label)
            match_t = re.search(LABELS_TARGETING, label)
            match_s = re.search(LABELS_SWEPT_FROM, label)
            if match_r:
                self.review_labels.append(label)
            elif match_t:
                self.other_targets.append(match_t.group(1))
                self.other_targets_labels.append(label)
            elif match_s:
                self.swept_from = match_s.group(1)
                self.swept_from_label = label
            else:
                self.other_labels.append(label)
        self.urgent = LABEL_URGENT in self.labels

        # Review state:
        if self.wip:
            self.state = 'wip'
        elif len(self.review_labels) > 1:
            self.state = 'multiple'
        elif self.review_labels:
            self.state = self.review_labels[0]
        else:
            self.state = 'none'
        self.approved = LABEL_APPROVED in self.review_labels
        self.waiting = LABEL_WAITING in self.review_labels
        self.l1 = LABEL_L1 in self.review_labels
        self.l2 = LABEL_L2 in self.review_labels
        self.expert = LABEL_EXPERT in self.review_labels
        self.review_pending = (
                (not self.wip) and
                (not self.waiting) and
                (not self.approved))

        self._bot_commented = None

    def get_fork(self, cgl):
        fork_id = self._raw.source_project_id
        try:
            return cgl.projects.get(fork_id)
        except GitlabGetError:
            return None

    def get_comments(self, cgl):
        return self._raw.notes.list(as_list = False)

    def check_bot_has_commented(self, cgl):
        if self._bot_commented is not None:
            return self._bot_commented
        for comment in self.get_comments(cgl):
            if comment.author['username'] == ATLAS_BOT:
                self._bot_commented = True
                return True
        self._bot_commented = False
        return False
