#!/usr/bin/python

# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Author: Carl Suster <carl.suster@cern.ch>
# Works with Python 2.7 or 3.
# Depends: python-gitlab, matplotlib (>= 2.0.0), numpy
#
# Data on the number of open MRs in atlas/athena is collected and plotted.
# The results are recorded separately for each branch, though currently the
# information is not plotted at this level of granularity.

from __future__ import print_function, with_statement

import sys, os
import time, datetime, calendar
import argparse
import logging

try: input = raw_input
except NameError: pass

import gitlab
import numpy as np
import matplotlib as mpl
mpl.use('Agg') # Force PyPlot to avoid accessing a (nonexistent) X server
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.patches as mpatches

from gitlab_api_helpers import AtlasMergeRequest


def get_occupancy(project):
    ''' Returns a timestamp and a map of categories to counts. '''

    data = {}
    for mr in project.mergerequests.list(state = 'opened', as_list = False):
        mr = AtlasMergeRequest(mr)
        category = (mr.state, mr.target_branch)
        if category not in data:
            data[category] = 0
        data[category] += 1
    return (time.gmtime(), data)

def serialise(date, data):
    ''' Turns the output of get_occupancy() into a string for storage. '''

    lines = []
    epoch = calendar.timegm(date)
    for (label, branch), count in data.items():
        lines.append('{}\t{}\t{}\t{}'.format(epoch, label, branch, count))
    return '\n'.join(lines) + '\n'

def tabulater(table):
    ''' Helper to convert an enum of labels into integers.
    Matplotlib's parsing function expects numbers, but we store categories by
    their labels. This just builds a list of labels in the order it encounters
    them so that each is uniquely associated to a number.
    '''

    def tabulate(string):
        if string in table:
            return table.index(string)
        else:
            table.append(string)
            return table.index(string)

    return tabulate

def decode_date(bstring):
    ''' Dates are stored in UTC; this converts it to a local matplotlib date. '''

    utc_epoch = float(bstring.decode('utf8'))
    return mdates.date2num(datetime.datetime.fromtimestamp(utc_epoch))

def deserialise_io(store, labels, branches):
    ''' Parses data into an ndarray, using label and branch tables provided. '''

    data = np.loadtxt(store, unpack=True, converters={
            0: decode_date,
            1: tabulater(labels),
            2: tabulater(branches)})
    return data

def command_fetch(site, token, project_name, data_store):
    ''' Sample the queue occupancy and store the result. '''

    cgl = gitlab.Gitlab(site, token, api_version = 4)
    project = cgl.projects.get(project_name, lazy=True)
    logging.debug("retrieved GitLab project handle")

    # Get the data:
    (date, data) = get_occupancy(project)

    # Append it to the appropriate file:
    store = os.path.join(data_store, time.strftime('mr-%Y-%m.dat', date))
    try:
        with open(store, 'a') as store_io:
            result = serialise(date, data)
            store_io.write(result)
        logging.info('Appended occupancy data to: {}'.format(store))
    except IOError:
        logging.exception('Failed to write data.')
        return 1

def command_plot(data_store, results_store):
    ''' Produce plots from the stored data. '''
    plt.ioff() # batch mode
    plt.figure(figsize=(20, 8))

    # Parse data from every file:
    # NOTE: In future could restrict to e.g. current + previous month only.
    ltable = []
    btable = []
    data = None
    for store in os.listdir(data_store):
        if store.endswith('.dat'):
            store = os.path.join(data_store, store)
            if not os.path.isfile(store):
                continue
            with open(store, 'r') as store_io:
                try:
                    data_tmp = deserialise_io(store_io, ltable, btable)
                except (UnicodeEncodeError, ValueError) as err:
                    logging.error('Problem decoding file: {}'.format(store))
                    if isinstance(err, ValueError):
                        logging.info('Failure reason: {}'.format(err.message))

                    # Attempt to find any problematic lines
                    prev_epoch = None
                    store_io.seek(0)
                    for lineno, line in enumerate(store_io.readlines()):
                        if line.count('\t') != 3:
                            logging.error('Wrong number of fields (lineno {}): {}'.format(
                                lineno + 1, repr(line)))
                            continue
                        timestamp, label, branch, count = line.split('\t')
                        try:
                            epoch = int(timestamp)
                        except ValueError:
                            logging.error('Invalid timestamp (lineno {}): {}'.format(
                                lineno + 1, repr(line)))
                            continue
                        if lineno > 0 and epoch < prev_epoch:
                            logging.warning('Timestamp not monotonic ({} < {}) (lineno {}): {}'.format(
                                epoch, prev_epoch, lineno + 1, repr(line)))
                        prev_epoch = epoch
                    logging.info('Try deleting corrupt lines from the data file mentioned')
                    sys.exit(1)

                if data is None:
                    data = data_tmp
                else:
                    data = np.concatenate((data, data_tmp), axis=1)

    # Label the individual columns:
    date, label, branch, count = data

    # Get a list of dates for plotting without gaps:
    all_dates = np.unique(date)

    for lab in sorted(ltable):
        # Get the integer associated with the label from the parse table:
        lid = ltable.index(lab)

        # Sum the counts for this label for each date in the dataset:
        reduced = data[:,label == lid]
        rdate = reduced[0]
        ucount = [np.sum(reduced[3,rdate == d]) for d in all_dates]

        # Plot using a date axis:
        plt.plot_date(x=all_dates, y=ucount, fmt='-',
                label=lab.decode('utf-8'))

    # Also plot the totals by repeating without the label filter step:
    ucount = [np.sum(data[3,date == d]) for d in all_dates]
    plt.plot_date(x=all_dates, y=ucount,
            fmt='k-',
            label='total')

    plt.ylabel('Open MRs')
    plt.grid(True)

    now = datetime.datetime.now()

    # Create legend (with timestamp added as a fake entry):
    handles, labels = plt.gca().get_legend_handles_labels()
    text = now.strftime('%Y-%m-%d %H:%M (CERN)')
    handles.append(mpatches.Patch(color='none', label=text))
    plt.legend(handles=handles)

    try:
        # All time:
        plt.title('MR queue occupancy')
        path = os.path.join(results_store, 'mr-queues-alltime.png')
        plt.savefig(path, bbox_inches='tight')

        # Past month:
        month = datetime.timedelta(30)
        plt.title('MR queue occupancy (30 days)')
        plt.xlim((now - month, now))
        path = os.path.join(results_store, 'mr-queues-month.png')
        plt.savefig(path, bbox_inches='tight')

        # Past week:
        week = datetime.timedelta(7)
        plt.title('MR queue occupancy (7 days)')
        plt.xlim((now - week, now))
        path = os.path.join(results_store, 'mr-queues-week.png')
        plt.savefig(path, bbox_inches='tight')
        return 0

    except IOError:
        logging.exception('Failed to update plots.')
        return 1


def main():
    parser = argparse.ArgumentParser(description="GitLab queue monitor",formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p','--project-name',dest='project_name',help="GitLab project with namespace (e.g. user/my-project)")
    parser.add_argument('-t','--token',help="private GitLab user token")
    parser.add_argument('-u','--url',default='https://gitlab.cern.ch',help="URL of GitLab instance")
    parser.add_argument('--data',default='./mr-queue-occupancy',help="Directory to store queue occupancy data")
    parser.add_argument('--results',default='./mr-queue-results',help="Directory to store queue occupancy plots and HTML")
    parser.add_argument('-v','--verbose',default='INFO',choices=['DEBUG','INFO','WARNING','ERROR','CRITICAL'],help="verbosity level")
    parser.add_argument('command',choices=['fetch', 'plot'],help="action to perform (fetch updates the data file using the API, plot updates the plots offline)")

    # get command line arguments
    args = parser.parse_args()

    # configure log output
    logging.basicConfig(format='%(asctime)s %(levelname)-10s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.getLevelName(args.verbose))

    logging.debug("parsed arguments:\n" + repr(args))

    # delegate
    if args.command == 'fetch':
        # Only when fetching do we need to interact with the API
        if not args.token:
            raise Exception('Token required for fetching')
        if not args.project_name:
            raise Exception('Project name required for fetching')
        return command_fetch(args.url, args.token, args.project_name, args.data)
    elif args.command == 'plot':
        return command_plot(args.data, args.results)
    else:
        raise Exception('Unknown command "{}"'.format(args.command))

if __name__ == '__main__':
    sys.exit(main())
